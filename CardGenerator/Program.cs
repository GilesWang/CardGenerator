﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入希望logo偏移的像素，如10");
            var s = Console.ReadLine();
            int c = int.Parse(s);
            var path = AppDomain.CurrentDomain.BaseDirectory + "/pngs/";
            var filesPath = AppDomain.CurrentDomain.BaseDirectory + "/files/";
            var logoR = Image.FromFile(filesPath + "logo_right.png");
            var logoL = Image.FromFile(filesPath + "logo_left.png");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            var names = getNames();
            foreach (var name in names)
            {
                Console.WriteLine("正在为"+name+"生成席卡");
                var img_ = CreateImage(name, false, 80);
                var img_2 = CombinImage(img_, logoL, c, 20);
                var img_3 = CombinImage(img_2, logoR, img_.Width - c - logoR.Width, 20);
                img_3.RotateFlip(RotateFlipType.Rotate90FlipNone);

                var img = CreateImage(name, false, 80);
                var img2 = CombinImage(img, logoL, c, 20);
                var img3 = CombinImage(img2, logoR, img.Width - c - logoR.Width, 20);
                img3.RotateFlip(RotateFlipType.Rotate270FlipNone);

                var b=CombineImg(img_3, img3);
                b.Save(path + name + Guid.NewGuid() + ".png");
            }
            Console.WriteLine("文件已全部生成至pngs文件夹,是不是很厉害 (: 请按任意键结束");
            Console.Read();
        }

        static string[] getNames()
        {
            var path = AppDomain.CurrentDomain.BaseDirectory + "/files/names.txt";
            var names = File.ReadAllText(path);
            return names.Split(',');
        }
        /// <summary>
        /// 生成文字图片
        /// </summary>
        /// <param name="text"></param>
        /// <param name="isBold"></param>
        /// <param name="fontSize"></param>
        static Image CreateImage(string text, bool isBold, int fontSize)
        {
            int wid = 800;
            int high = 400;
            Font font;
            if (isBold)
            {
                font = new Font("Arial", fontSize, FontStyle.Bold);

            }
            else
            {
                font = new Font("Arial", fontSize, FontStyle.Regular);

            }
            //绘笔颜色
            SolidBrush brush = new SolidBrush(Color.Black);
            StringFormat format = new StringFormat(StringFormatFlags.NoClip);
            Bitmap image = new Bitmap(wid, high);
            Graphics g = Graphics.FromImage(image);
            SizeF sizef = g.MeasureString(text, font, PointF.Empty, format);//得到文本的宽高
            int width = (int)(sizef.Width + 1);
            int height = (int)(sizef.Height + 1);
            image.Dispose();
            image = new Bitmap(wid, high);
            g = Graphics.FromImage(image);
            g.TextRenderingHint = TextRenderingHint.AntiAlias;
            g.Clear(Color.White);//透明

            RectangleF rect = new RectangleF((wid - width) / 2, (high - height) / 2, width, height);
            //绘制图片
            g.DrawString(text, font, brush, rect);
            
            //释放对象
            g.Dispose();
            return image;
        }

        /// <summary>  
        /// 合并图片  
        /// </summary>  
        /// <param name="imgBack"></param>  
        /// <param name="img"></param>  
        /// <returns></returns>  
        static Bitmap CombinImage(Image imgBack, Image img, int xDeviation = 0, int yDeviation = 0)
        {

            Bitmap bmp = new Bitmap(imgBack.Width, imgBack.Height);

            Graphics g = Graphics.FromImage(bmp);
            g.Clear(Color.White);
            g.DrawImage(imgBack, 0, 0, imgBack.Width, imgBack.Height);

            g.DrawImage(img, xDeviation, yDeviation, img.Width, img.Height);
            GC.Collect();
            return bmp;
        }
        static Image CombineImg(Image a, Image b)
        {
            Bitmap bmp = new Bitmap(a.Width + b.Width+20, a.Height);
            Graphics g = Graphics.FromImage(bmp);
            g.Clear(Color.White);
            g.DrawImage(a, 0, 0, a.Width, a.Height);

            g.DrawImage(b, a.Width, 0, b.Width, b.Height);
            GC.Collect();
            return bmp;
        }
    }
}
